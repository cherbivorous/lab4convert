package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float interestRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.btnConvertInt);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);
		
		TextView intRate = (TextView)findViewById(R.id.intRate);
		interestRate = Float.parseFloat(intRate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.btnConvertInt) {
			EditText deposit = (EditText)findViewById(R.id.editDeposit);
			EditText year = (EditText)findViewById(R.id.editYear);
			TextView result = (TextView)findViewById(R.id.result);
			String res = "";
			
			try {
				float Dep = Float.parseFloat(deposit.getText().toString());
				int yy = Integer.parseInt(year.getText().toString());
				double amount = Dep * (Math.pow((interestRate*0.01) + 1, yy));
				
				res = String.format(Locale.getDefault(), "%.2f", amount);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			result.setText(res);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", interestRate);
			startActivityForResult(i, 9999);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("interestRate", 32.0f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", interestRate));
		}
	}

	

}
